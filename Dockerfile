FROM composer:1.6.5 as build
WORKDIR /src
COPY /src /src
RUN composer install

FROM php:7.1.8-apache
RUN apt-get update && apt-get install -y \
        libpng-dev \
        zlib1g-dev \
        libxml2-dev \
        libzip-dev \
        libonig-dev \
        zip \
        curl \
        unzip \
    && docker-php-ext-configure gd \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install mysqli \
    && docker-php-ext-install zip \
    && docker-php-source delete
COPY --from=build --chown=www-data:www-data /src /var/www/html
COPY vhost.conf /etc/apache2/sites-available/000-default.conf
#RUN ls -la /var/www/html/public
RUN php artisan key:generate
RUN php artisan jwt:generate
RUN a2enmod rewrite
EXPOSE 80
